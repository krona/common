<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 9/11/14
 * Time: 11:05 AM
 */

namespace Krona\Common\Common\Converter;


class IntegerConverter implements ConverterInterface
{

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        if (!is_null($value)) {
            return (int)$value;
        } else {
            return null;
        }
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        return $value;
    }
}