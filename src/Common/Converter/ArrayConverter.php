<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 17.08.14
 * Time: 6:43
 */

namespace Krona\Common\Common\Converter;

/**
 * ArrayConverter - used for convert array to Json when saving to DB
 * @package Krona\Common\Common\Converter
 */
class ArrayConverter implements ConverterInterface
{
    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        if (is_array($value)) {
            return $value;
        }
        return json_decode($value, true);
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        return json_encode($value);
    }
}