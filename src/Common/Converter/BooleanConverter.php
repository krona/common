<?php
/**
 * User: krona
 * Date: 11/10/14
 * Time: 4:56 AM
 */

namespace Krona\Common\Common\Converter;


class BooleanConverter implements ConverterInterface
{

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        if (is_null($value)) {
            return null;
        }
        return !!($value);
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        if (is_null($value)) {
            return null;
        }
        return ($value)?1:0;
    }
}