<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 9/5/14
 * Time: 12:44 PM
 */

namespace Krona\Common\Common\Converter;

use Krona\Common\Object\ObjectInterface;

/**
 * AssociationConverter - revert association Object to Identifier while saving to DB
 * @package Krona\Common\Common\Converter
 */
class AssociationConverter implements ConverterInterface
{

    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        return $value;
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        if ($value == '') {
            return null;
        } elseif ($value instanceof ObjectInterface) {
            return (int)$value->getId();
        } else {
            return (int)$value;
        }
    }
}