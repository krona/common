<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 14.08.14
 * Time: 1:34
 */

namespace Krona\Common\Common\Converter;

/**
 * Interface ConverterInterface
 * @package Krona\Common\Common\Converter
 */
interface ConverterInterface
{
    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value);

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value);
} 