<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 17.08.14
 * Time: 6:53
 */

namespace Krona\Common\Common\Converter;

use Krona\Common\Object\ObjectInterface;

/**
 * DefaultConverter default converter for other types
 * @package Krona\Common\Common\Converter
 */
class DefaultConverter implements ConverterInterface
{
    /**
     * Convert to PHP type
     * @param $value
     * @return mixed
     */
    public function convert($value)
    {
        return $value;
    }

    /**
     * Convert to SQL type
     * @param $value
     * @return mixed
     */
    public function revert($value)
    {
        if ($value instanceof ObjectInterface) {
            return $value->getId();
        }
        return $value;
    }
}