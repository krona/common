<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 14.08.14
 * Time: 1:28
 */

namespace Krona\Common\Common;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Krona\Common\Common\Converter\ConverterInterface;
use Krona\Common\Common\Converter\DefaultConverter;
use Krona\Common\Object\ObjectInterface;
use Krona\Common\ObjectManager;
use ReflectionProperty;
use Zend\Stdlib\Hydrator\HydratorInterface;

/**
 * Class ObjectHydrator
 * @package Krona\Common\Common
 */
class ObjectHydrator implements HydratorInterface
{
    /** @var  ObjectManager */
    protected $em;
    /** @var  ClassMetadata */
    protected $metadata;
    /** @var  array */
    protected $convertersConfig;
    /** @var  ConverterInterface */
    protected $converters = [];

    /**
     * @param ObjectManager $em
     * @param array         $config
     */
    public function __construct(ObjectManager $em, $config)
    {
        $this->em = $em;
        $this->convertersConfig = $config['krona']['types']['converters'];
    }

    /**
     * Extract values from an object
     *
     * @param  object $object
     * @param bool    $asColumns
     * @param int     $depth
     * @internal param bool $column
     * @return array
     */
    public function extract($object, $asColumns = true, $depth = 5)
    {
        $this->prepare($object);

        $result = [];
        /** @var ReflectionProperty $property */
        foreach ($this->metadata->getReflectionProperties() as $property) {
            $property->setAccessible(true);
            try {
                $columnName = $this->getColumnName($property);
            } catch (\Exception $e) {
                //
                continue;
            }
            $converter = $this->getConverter($property);
            if ($asColumns) {
                if (!is_null($columnName)) {
                    $result[$columnName] = $converter->revert($property->getValue($object), $asColumns);
                }
            } else {
                if ($property->getValue($object) instanceof ObjectInterface && $depth > 0) {
                    $result[$property->getName()] = $this->extract($property->getValue($object), false, $depth - 1);
                } else {
                    $result[$property->getName()] = $converter->revert($property->getValue($object), $asColumns);
                }
            }
        }

        return $result;
    }

    /**
     * @param $object
     */
    protected function prepare($object)
    {
        $this->metadata = $this->em->getClassMetadata(get_class($object));
    }

    /**
     * @param ReflectionProperty $property
     * @return null|string
     */
    protected function getColumnName(ReflectionProperty $property)
    {
        return ($this->metadata->hasField($property->getName())) ? $this->metadata->getColumnName(
            $property->getName()
        ) : ($this->metadata->hasAssociation(
            $property->getName()
        ) ? $this->metadata->getSingleAssociationJoinColumnName($property->getName()) : null);
    }

    /**
     * @param ReflectionProperty $property
     * @return ConverterInterface
     */
    protected function getConverter(ReflectionProperty $property)
    {
        if ($this->metadata->hasAssociation($property->getName())) {
            $type = 'association';
        } else {
            $type = $this->metadata->getTypeOfField($property->getName());
        }
        if (!isset($this->converters[$type])) {
            if (isset($this->convertersConfig[$type])) {
                $converter = $this->convertersConfig[$type];
            } else {
                $converter = DefaultConverter::class;
            }
            $this->converters[$type] = new $converter();
        }

        return $this->converters[$type];
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array  $data
     * @param  object $object
     * @param bool    $asColumns
     * @return object
     */
    public function hydrate(array $data, $object, $asColumns = true)
    {
        $this->prepare($object);

        /** @var ReflectionProperty $property */
        foreach ($this->metadata->getReflectionProperties() as $property) {
            if ($asColumns) {
                try {
                    $columnName = $this->getColumnName($property);
                } catch (\Exception $e) {
                    continue;
                }
            } else {
                $columnName = $property->getName();
            }
            $accessible = $property->isPublic();
            if (!$accessible) {
                $property->setAccessible(true);
            }
            $converter = $this->getConverter($property);
            if (!is_null($columnName) && isset($data[$columnName])) {
                if (!is_null($property->getValue($object))) {
                    $old = $property->getValue($object);
                    if ($old instanceof ObjectInterface) {
                        $value = $converter->convert($data[$columnName]);
                        if ($old->getId() != $value) {
                            $property->setValue(
                                $object,
                                $value
                            );
                        }
                    } else {
                        $property->setValue(
                            $object,
                            $converter->convert($data[$columnName])
                        );
                    }
                } else {
                    $property->setValue(
                        $object,
                        $converter->convert($data[$columnName])
                    );
                }
            }
            if (!$accessible) {
                $property->setAccessible(false);
            }
        }

        return $object;
    }
}