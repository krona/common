<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 14.08.14
 * Time: 2:02
 */

namespace Krona\Common\Common;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Krona\Common\Object\ObjectInterface;
use Krona\Common\ObjectManager;
use Krona\Common\Repository\ObjectRepository;

/**
 * Class UnitOfWork
 * @package Krona\Common\Common
 */
class UnitOfWork
{
    /** @var ObjectInterface[] */
    protected $instances = [];
    /** @var array[] */
    protected $origins = [];
    /** @var ClassMetadata */
    protected $classMetadata;
    /** @var ObjectManager */
    protected $em;
    /** @var  string */
    protected $className;

    public function __construct(ObjectRepository $repository)
    {
        $this->classMetadata = $repository->getClassMetadata();
        $this->className = $this->classMetadata->getName();
        $this->em = $repository->getObjectManager();
    }

    /**
     * remove Entity from Unit Of Work
     * @param $id
     */
    public function remove($id)
    {
        unset($this->instances[$id]);
        unset($this->origins[$id]);
    }

    /**
     * Hydrate raw data to Object
     * @param array $data
     * @return ObjectInterface
     */
    public function hydrate(array $data)
    {
        if (isset($data['__document_class'])) {
            $className = $data['__document_class'];
        } else {
            $className = $this->className;
        }
        /** @var ObjectInterface $object */
        $object = $this->em->getObjectHydrator()->hydrate(
            $data,
            new $className
        );

        if ($this->has($object->getId())) {
            $object = $this->merge($object->getId(), $data);
        } else {
            $this->push($object);
        }

        return $object;
    }

    /**
     * Check that UnitOfWork manage some entity by his Identifier
     * @param $id
     * @return bool
     */
    public function has($id)
    {
        return isset($this->instances[$id]);
    }

    /**
     * Method that used to merge data in UnitOfWork and DB
     * @param       $id
     * @param array $data
     * @return ObjectInterface|bool
     */
    protected function merge($id, array $data)
    {
        $entity = $this->instances[$id];
        $this->em->getObjectHydrator()->hydrate($data, $entity);
        $this->register($id, $data);

        return $entity;
    }

    /**
     * Method for registering origins of Fetched data from DB
     * @param $id
     * @param $data
     */
    protected function register($id, $data)
    {
        $this->origins[$id] = $data;
    }

    /**
     * Method used for add Object to Managing by UnitOfWork
     * @param ObjectInterface $entity
     */
    public function push(ObjectInterface $entity)
    {
        $this->instances[$entity->getId()] = $entity;

        if (!isset($this->origins[$entity->getId()])) {
            $this->register(
                $entity->getId(),
                $this->em->getObjectHydrator()->extract($entity)
            );
        }
    }

    /**
     * Method used for getting Object by Identifier that managed by UnitOfWork
     * @param $id
     * @return ObjectInterface|bool
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->instances[$id];
        } else {
            return false;
        }
    }

    /**
     * Method returns origin data, that received from DB
     * @param $id
     * @return array|null
     */
    public function getOrigin($id)
    {
        if (!$this->has($id)) {
            return null;
        }
        return $this->origins[$id];
    }

    /**
     * Method used for updateOrigin by Object
     * @param ObjectInterface $entity
     * @return $this
     */
    public function updateOrigin(ObjectInterface $entity)
    {
        $this->instances[$entity->getId()] = $entity;
        $this->origins[$entity->getId()] = $this->em->getObjectHydrator()->extract($entity);

        return $this;
    }

    /**
     * Method used for calculating all changes for managed Objects
     * @return array
     */
    public function computeChangeSets()
    {
        $entitiesToProcess = [];
        foreach ($this->instances as $id => $entity) {
            $changes = $this->calculateChanges($entity);
            if (!empty($changes)) {
                $entitiesToProcess[$id] = [
                    $entity,
                    $changes,
                ];
            }
        }

        return $entitiesToProcess;
    }

    /**
     * Method returns an array of changes in Object that managed by UnitOfWork
     * @param ObjectInterface $entity
     * @return array
     */
    public function calculateChanges(ObjectInterface $entity)
    {
        if (!is_null($entity->getId()) && isset($this->origins[$entity->getId()])) {
            $origin = $this->origins[$entity->getId()];
            return $this->arrayDiff(
                $this->em->getObjectHydrator()->extract($entity),
                $origin
            );
        } else {
            return $this->em->getObjectHydrator()->extract($entity);
        }
    }

    /**
     * Fix for working with int => string diff
     * @param $current
     * @param $origin
     * @return array
     */
    protected function arrayDiff($current, $origin)
    {
        $diff = [];

        foreach ($current as $key => $value) {
            if ($value . '' !== (isset($origin[$key]) ? $origin[$key] : '') . '') {
                $diff[$key] = $value;
            }
        }

        return $diff;
    }

    /**
     * Clear managed Objects
     */
    public function clear()
    {
        $this->origins = [];
        $this->instances = [];
    }
}