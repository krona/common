<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 9/29/14
 * Time: 4:57 PM
 */

namespace Krona\Common;

use Doctrine\Common\Persistence\ObjectManager as DoctrineObjectManager;
use Zend\Stdlib\Hydrator\HydratorInterface;

interface ObjectManager extends DoctrineObjectManager
{
    /**
     * @return HydratorInterface
     */
    public function getObjectHydrator();
} 