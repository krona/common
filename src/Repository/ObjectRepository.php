<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 9/29/14
 * Time: 5:01 PM
 */

namespace Krona\Common\Repository;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectRepository as DoctrineObjectRepository;
use Krona\Common\Object\ObjectInterface;
use Krona\Common\ObjectManager;

interface ObjectRepository extends DoctrineObjectRepository
{
    /**
     * Method used for saving some Object to DB
     * @param ObjectInterface $object
     * @return mixed
     */
    public function commit(ObjectInterface $object);

    /**
     * Method returns Metadata of managed Object
     * @return ClassMetadata
     */
    public function getClassMetadata();

    /**
     * @return ObjectManager
     */
    public function getObjectManager();
} 