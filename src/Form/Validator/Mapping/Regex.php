<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 21.08.14
 * Time: 1:11
 */

namespace Krona\Common\Form\Validator\Mapping;

use Krona\Common\Form\Validator\ValidatorMappingInterface;
use Zend\InputFilter\Input;

/**
 * Class Regex
 * @package Krona\Common\Form\Validator\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Regex implements ValidatorMappingInterface
{
    /** @var  string */
    public $pattern;
    /** @var  string */
    public $message;

    public function attach(Input $input)
    {
        $options = [
            'pattern' => $this->pattern,
        ];

        if (!is_null($this->message)) {
            $options['message'] = $this->message;
        }

        $input
            ->getValidatorChain()
            ->attach(new \Zend\Validator\Regex($options));
    }
}