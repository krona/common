<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 19.08.14
 * Time: 21:56
 */

namespace Krona\Common\Form\Validator\Mapping;


use Krona\Common\Form\Validator\ValidatorMappingInterface;
use Zend\InputFilter\Input;

/**
 * Class StringLength
 * @package Krona\Common\Form\Validator\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class StringLength implements ValidatorMappingInterface
{
    /** @var  integer */
    public $min = 0;
    /** @var  integer */
    public $max = null;
    /** @var  string */
    public $message;

    public function attach(Input $input)
    {
        $options = [
            'min' => $this->min,
            'max' => $this->max,
        ];

        if (!is_null($options)) {
            $options['message'] = $this->message;
        }

        $input->getValidatorChain()
            ->attach(new \Zend\Validator\StringLength($options));
    }
}