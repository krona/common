<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 4:56
 */

namespace Krona\Common\Form\Validator\Mapping;


use Krona\Common\Form\Validator\ValidatorMappingInterface;
use Zend\InputFilter\Input;

/**
 * Class Required
 * @package Krona\Common\Form\Validator\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Required implements ValidatorMappingInterface
{
    public function attach(Input $input)
    {
        $input->setRequired(true);
    }
}