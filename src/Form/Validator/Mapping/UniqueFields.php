<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 19.08.14
 * Time: 22:04
 */

namespace Krona\Common\Form\Validator\Mapping;

use Krona\Common\Form\Validator\UniqueObject;
use Krona\Common\Form\Validator\ValidatorClassMappingInterface;
use Krona\Common\ObjectManager;
use Zend\InputFilter\BaseInputFilter;

/**
 * Class UniqueFields
 * @package Krona\Common\Form\Validator\Mapping
 * @Annotation
 * @Target({"CLASS"})
 */
class UniqueFields implements ValidatorClassMappingInterface
{
    protected $fields = [];

    public function __construct($fields)
    {
        $fields = $fields['value'];
        if (!is_array($fields)) {
            $fields = [$fields];
        }
        $this->fields = $fields;
    }

    /**
     * @param BaseInputFilter $inputFilter
     * @param ObjectManager   $entityManager
     * @param                 $className
     */
    public function attach(BaseInputFilter $inputFilter, ObjectManager $entityManager, $className)
    {
        foreach ($this->fields as $field) {
            $inputFilter->get($field)
                ->getValidatorChain()
                ->attach(
                    new UniqueObject(
                        [
                            'objectManager' => $entityManager,
                            'checkField' => $field,
                            'objectClass' => $className,
                        ]
                    )
                );
        }
    }
}