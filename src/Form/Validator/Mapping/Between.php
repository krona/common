<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 21.08.14
 * Time: 1:09
 */

namespace Krona\Common\Form\Validator\Mapping;

use Krona\Common\Form\Validator\ValidatorMappingInterface;
use Zend\InputFilter\Input;

/**
 * Class Between
 * @package Krona\Common\Form\Validator\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Between implements ValidatorMappingInterface
{
    /** @var  integer */
    public $min = 0;
    /** @var  integer */
    public $max = null;
    /** @var  string */
    public $message;

    public function attach(Input $input)
    {
        $options = [
            'min' => $this->min,
            'max' => $this->max,
        ];

        if (!is_null($options)) {
            $options['message'] = $this->message;
        }

        $input
            ->getValidatorChain()
            ->attach(new \Zend\Validator\Between($options));
    }
}