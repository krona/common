<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 27.08.14
 * Time: 0:06
 */

namespace Krona\Common\Form\Validator;

use Krona\Common\ObjectManager;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class UniqueObject extends AbstractValidator
{
    const OBJECT_FOUND = 'objectFound';
    protected $messageTemplates = array(
        self::OBJECT_FOUND => 'There is object with this value'
    );

    /** @var  ObjectManager */
    protected $objectManager;
    /** @var  string */
    protected $objectClass;
    /** @var  string */
    protected $checkField;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @param null   $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);
        if (null === $this->getObjectManager()) {
            throw new Exception\RuntimeException(__METHOD__ . ' There is no objectManager set.');
        }

        if (null === $this->getObjectClass()) {
            throw new Exception\RuntimeException(__METHOD__ . ' There is no entity class name set.');
        }
        $metadata = $this->getObjectManager()->getClassMetadata($this->getObjectClass());
        $identifier = $metadata->getIdentifierFieldNames();
        $identifier = array_shift($identifier);
        if (null === $this->getCheckField()) {
            $this->setCheckField($identifier);
        }

        if (is_array($context) && isset($context[$identifier]) && !is_null($context[$identifier])) {
            $entity = $this
                ->getObjectManager()
                ->getRepository($this->getObjectClass())->findOneBy([$this->getCheckField() => $this->getValue()]);

            if ($entity && $entity->getId() != $context[$identifier]) {
                $this->error(self::OBJECT_FOUND);

                return false;
            } else {
                return true;
            }
        } else {
            $entity = $this
                ->getObjectManager()
                ->getRepository($this->getObjectClass())->findOneBy([$this->getCheckField() => $this->getValue()]);

            // Set Error message
            if ($entity) {
                $this->error(self::OBJECT_FOUND);

                return false;
            } else {
                return true;
            }
        }
    }

    public function getObjectManager()
    {
        return $this->objectManager;
    }

    public function setObjectManager($entityManager)
    {
        $this->objectManager = $entityManager;

        return $this;
    }

    /**
     * @return string
     */
    public function getObjectClass()
    {
        return $this->objectClass;
    }

    /**
     * @param string $entityClass
     * @return $this
     */
    public function setObjectClass($entityClass)
    {
        $this->objectClass = $entityClass;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheckField()
    {
        return $this->checkField;
    }

    /**
     * @param mixed $checkField
     * @return $this
     */
    public function setCheckField($checkField)
    {
        $this->checkField = $checkField;

        return $this;
    }
}