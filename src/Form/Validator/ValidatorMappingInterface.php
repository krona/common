<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 4:56
 */

namespace Krona\Common\Form\Validator;


use Zend\InputFilter\Input;

interface ValidatorMappingInterface
{
    public function attach(Input $input);
} 