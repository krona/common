<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 19.08.14
 * Time: 22:16
 */

namespace Krona\Common\Form\Validator;

use Krona\Common\ObjectManager;
use Zend\InputFilter\BaseInputFilter;

interface ValidatorClassMappingInterface
{
    public function attach(BaseInputFilter $inputFilter, ObjectManager $entityManager, $className);
} 