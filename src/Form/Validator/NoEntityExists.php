<?php
/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Created by JetBrains PhpStorm.
 * User: Filipp Qoma
 * Date: 08.10.13
 * Time: 19:17
 */

namespace Krona\Common\Form\Validator;

use Doctrine\Common\Persistence\ObjectManager as EntityManager;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

/**
 * Class NoEntityExists
 * @package Krona\Common\Validator
 */
class NoEntityExists extends AbstractValidator
{
    const ENTITY_FOUND = 'entityFound';
    protected $messageTemplates = array(
        self::ENTITY_FOUND => 'There is  entity with this value'
    );
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @param string
     */
    protected $entityClassName;
    /**
     * @var string
     */
    protected $checkField;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (null === $this->getEntityManager()) {
            throw new Exception\RuntimeException(__METHOD__ . ' There is no entityManager set.');
        }

        if (null === $this->getEntityClassName()) {
            throw new Exception\RuntimeException(__METHOD__ . ' There is no entity class name set.');
        }
        if (null === $this->getCheckField()) {
            $metadata = $this->getEntityManager()->getClassMetadata($this->getEntityClassName());
            $identifier = $metadata->getIdentifierFieldNames();
            $this->setCheckField(array_shift($identifier));
        }
        $this->setValue($value);

        $entity = $this
            ->getEntityManager()
            ->getRepository($this->getEntityClassName())->findOneBy([$this->getCheckField() => $this->getValue()]);

        // Set Error message
        if ($entity) {
            $this->error(self::ENTITY_FOUND);

            return false;
        } else {
            return true;
        }
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param  EntityManager $entityManager
     * @return NoEntityExists
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityClassName()
    {
        return $this->entityClassName;
    }

    /**
     * @param  string $entityClassName
     * @return NoEntityExists
     */
    public function setEntityClassName($entityClassName)
    {
        $this->entityClassName = $entityClassName;

        return $this;
    }

    /**
     * @return string
     */
    public function getCheckField()
    {
        return $this->checkField;
    }

    /**
     * @param  string $checkField
     * @return NoEntityExists
     */
    public function setCheckField($checkField)
    {
        $this->checkField = $checkField;

        return $this;
    }
}
