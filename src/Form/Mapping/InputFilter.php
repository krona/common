<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 10:00
 */

namespace Krona\Common\Form\Mapping;

/**
 * Class InputFilter
 * @package Krona\Common\Form\Mapping
 * @Annotation
 * @Target({"CLASS"})
 */
class InputFilter
{
    public $serviceName;
} 