<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 0:57
 */

namespace Krona\Common\Form\Filter;


use Zend\Filter\FilterChain;

interface FilterMappingInterface
{
    public function attach(FilterChain $filterChain);
} 