<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:12
 */

namespace Krona\Common\Form\Filter\Mapping;

use Krona\Common\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;
use Zend\Filter\HtmlEntities as BaseHtmlEntities;

/**
 * Class HtmlEntities
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class HtmlEntities implements FilterMappingInterface
{

    public function attach(FilterChain $filterChain)
    {
        $filterChain->attach(new BaseHtmlEntities());
    }
}