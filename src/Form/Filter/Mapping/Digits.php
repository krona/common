<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/11/14
 * Time: 11:07 AM
 */

namespace Krona\Common\Form\Filter\Mapping;


use Doctrine\Common\Annotations\Annotation\Target;
use Krona\Common\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;

/**
 * Class Digits
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Digits implements FilterMappingInterface
{

    public function attach(FilterChain $filterChain)
    {
        $filterChain->attach(new \Zend\Filter\Digits());
    }
}