<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 0:55
 */

namespace Krona\Common\Form\Filter\Mapping;

use Krona\Common\Form\Filter\FilterMappingInterface;
use Zend\Filter\FilterChain;
use Zend\Filter\StringTrim as BaseStringTrim;

/**
 * Class StringTrim
 * @package Krona\Common\Form\Filter\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class StringTrim implements FilterMappingInterface
{
    public function attach(FilterChain $filterChain)
    {
        $filterChain->attach(new BaseStringTrim());
    }
}