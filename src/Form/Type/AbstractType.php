<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 17.08.14
 * Time: 10:35
 */

namespace Krona\Common\Form\Type;

use Krona\Common\Form\Exception\TypeException;
use Zend\InputFilter\BaseInputFilter;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;

class AbstractType extends BaseInputFilter implements HydratorAwareInterface
{
    protected $hydrator;

    protected $rawObject;

    protected $object;

    protected $dataClass;

    public function getDataClass()
    {
        return $this->dataClass;
    }

    public function setDataClass($dataClass)
    {
        $this->dataClass = $dataClass;

        return $this;
    }

    protected function populate()
    {
        parent::populate();
        $this->injectObject();

        $this->setRawObject(
            $this->getHydrator()->hydrate(
                $this->getRawValues(),
                $this->getRawObject(),
                false
            )
        );
        $this->setObject(
            $this->getHydrator()->hydrate(
                $this->getValues(),
                $this->getObject(),
                false
            )
        );
    }

    protected function injectObject()
    {
        if (is_null($this->object) && is_null($this->rawObject)) {
            if ($this->dataClass != '') {
                $this->bind(
                    new $this->dataClass()
                );
            } else {
                throw new TypeException(
                    'You must bind or provide dataClass for using Type'
                );
            }
        }
    }

    public function bind($object)
    {
        $this->rawObject = $object;
        $this->object = clone $object;
    }

    /**
     * Retrieve hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        return $this->hydrator;
    }

    /**
     * Set hydrator
     *
     * @param  HydratorInterface $hydrator
     * @return HydratorAwareInterface
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    public function getRawObject()
    {
        return $this->rawObject;
    }

    public function setRawObject($rawObject)
    {
        $this->rawObject = $rawObject;
    }

    public function getObject()
    {
        return $this->object;
    }

    public function setObject($object)
    {
        $this->object = $object;
    }
}