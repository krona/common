<?php
/**
 * User: krona
 * Date: 10/3/14
 * Time: 4:45 PM
 */

namespace Krona\Common\Service;


use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Krona\Common\Form\Filter\FilterMappingInterface;
use Krona\Common\Form\Mapping\InputFilter as InputFilterAnnotation;
use Krona\Common\Form\Type\AbstractType;
use Krona\Common\Form\Validator\ValidatorClassMappingInterface;
use Krona\Common\Form\Validator\ValidatorMappingInterface;
use Krona\Common\ObjectManager;
use ReflectionProperty;
use Zend\InputFilter\Input;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class InputFilterGenerator implements ServiceLocatorAwareInterface
{
    /** @var  ServiceLocatorInterface */
    protected $serviceLocator;
    /** @var  ObjectManager */
    protected $objectManager;
    /** @var  ClassMetadata */
    protected $classMetadata;
    /** @var  AnnotationReader */
    protected $reader;
    /** @var  AbstractType */
    protected $filter;
    /** @var  ObjectManager */
    protected $usedEntityManager;

    public function createInputFilter($object, ObjectManager $objectManager)
    {
        if (is_string($object)) {
            $object = new $object();
        }
        $this->objectManager = $objectManager;
        $this->reader = $this->objectManager->getMetadataFactory()->getReader();

        $this->prepare($object);
        $this->fetchInputFilter();
        $this->parseFieldsAnnotations();
        $this->parseClassAnnotations();
        $this->prepareData($object);

        return $this->filter;
    }

    /**
     * Fetch Metadata for Object
     * @param object $object
     */
    protected function prepare($object)
    {
        $this->classMetadata = $this->objectManager->getClassMetadata(get_class($object));
    }

    /**
     * Create InputFilter based on Annotation or create AbstractType
     */
    protected function fetchInputFilter()
    {
        /** @var null|InputFilterAnnotation $annotation */
        $annotation = $this->reader->getClassAnnotation(
            $this->classMetadata->getReflectionClass(),
            InputFilterAnnotation::class
        );
        if (!is_null($annotation)) {
            $this->filter = $this->getServiceLocator()->get($annotation->serviceName);

            if ($this->filter instanceof AbstractType) {
                $this->filter
                    ->setDataClass($this->classMetadata->getName())
                    ->setHydrator($this->objectManager->getObjectHydrator());
            }
        } else {
            $this->filter = new AbstractType();
            $this->filter
                ->setDataClass($this->classMetadata->getName())
                ->setHydrator($this->objectManager->getObjectHydrator());
        }
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $config = $this->getServiceLocator()->get('Config');
        if (isset($config['krona']['form']['em']) && $config['krona']['form']['em'] != '') {
            $this->usedEntityManager = $this->getServiceLocator()->get(
                $config['krona']['form']['em']
            );
        }
    }

    /**
     * Parse Fields Annotations for adds Validators and Filters
     */
    protected function parseFieldsAnnotations()
    {
        foreach ($this->classMetadata->reflFields as $field) {
            $annotations = $this->reader->getPropertyAnnotations($field);
            $input = $this->injectInput($field);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof FilterMappingInterface) {
                    $annotation->attach($input->getFilterChain());
                } elseif ($annotation instanceof ValidatorMappingInterface) {
                    $annotation->attach($input);
                }
            }
        }
    }

    /**
     * @param ReflectionProperty $property
     * @return Input
     */
    protected function injectInput(ReflectionProperty $property)
    {
        if (!$this->filter->has($property->getName())) {
            $input = new Input($property->getName());
            $input->setRequired(false);
            $this->filter->add($input);
        }

        return $this->filter->get($property->getName());
    }

    /**
     * Parse Class Annotations for adds Validators
     */
    protected function parseClassAnnotations()
    {
        $annotations = $this->reader->getClassAnnotations($this->classMetadata->getReflectionClass());
        foreach ($annotations as $annotation) {
            if ($annotation instanceof ValidatorClassMappingInterface) {
                $annotation->attach(
                    $this->filter,
                    $this->usedEntityManager ? $this->usedEntityManager : $this->objectManager,
                    $this->classMetadata->getName()
                );
            }
        }
    }

    /**
     * Place current data from Object
     * @param object $object
     */
    protected function prepareData($object)
    {
        $this->filter->setData(
            $this->objectManager->getObjectHydrator()->extract($object, false)
        );
    }
} 