<?php
/**
 * User: Alex Grand <Kiral.Group@gmail.com>
 * Date: 9/29/14
 * Time: 4:56 PM
 */

namespace Krona\Common\Object;


interface ObjectInterface
{
    public function getId();

    public function setId($id);
} 